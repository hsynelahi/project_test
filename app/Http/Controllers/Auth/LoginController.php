<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserLoginRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    public function login(UserLoginRequest $request)
    {
        //validate request
        $validatedData = $request->validated();

        //check email
        $user = User::where('email', $validatedData['email'])->first();

        if (!$user) return $this->errorResponse();


        //check passowrd
        if (!Hash::check($validatedData['password'], $user->password)) return $this->errorResponse();
        

        //create token
        $createToken = $user->createToken('authToken');


        $data = [
            'user' => $user,
            'access_token' => $createToken->accessToken,
            'token_type' => 'Bearer',
            'expiered_at' => Carbon::parse($createToken->token->expires_at),
        ];

        return response()->json($data, 200);

    }

    private function errorResponse()
    {
        return response()->json([
            'status' => 'error',
            'message' => 'Forbiden : Incorrect email or password',
        ], 403);
    }

}
