<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\CodeRequest;
use App\Http\Requests\Auth\UserRegisterRequest;
use App\Mail\SendCodeRegister;
use App\Models\EmailRegisterCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function regisster(UserRegisterRequest $request)
    {
        //validate request
        $validatedData = $request->validated();

        //check email is already taken or not
        if (User::where('email', $validatedData['email'])->exists()) return response(['status' => 'error', 'message' => 'this email is already registered.'], 400);

        //create user
        $user = User::create([
            'name' => $validatedData['name'],
            'family' => $validatedData['family'],
            'password' => Hash::make($validatedData['password']),
        ]);

        //create token
        $createdToken = $user->createToken('authToken'); //config('license.token.types.bearer.tag'), ['*'], Carbon::now()->addMinute()

        //send notification
        //todo send welcome notification

        //return response
        return response(['data' => [
            'user' => $user,
            'token' => $createdToken->plainTextToken,
            'access_token' => $createdToken->accessToken,
            'token_type' => 'Bearer',
            'expiered_at' => Carbon::parse($createdToken->token->expires_at),
        ]]);
    }

    public function sendRegisterCode(UserRegisterRequest $request)
    {
        //validate email from request 
        $validatedData = $request->validated();

        //generate code
        $code = $this->generateCode();

        //save to database
        EmailRegisterCode::create([
            'code' => $code,
            'email' => $validatedData['email'],
        ]);

        //send code with email
        $this->sendEmail($validatedData, $code);

        //return response
        return response()->json(['status' => 'success', 'message' => 'Register Code sent Successfully!'], 200);
    }

    public function register(CodeRequest $request)
    {
        $validatedData = $request->validated();

        $code = EmailRegisterCode::where('code', $validatedData['code'])->first();

        if ($validatedData['code'] === $code->code) {
            User::create([
                'name' => $validatedData['name'],
                'family' => $validatedData['family'],
                'email' => $validatedData['email'],
                'password' => Hash::make($validatedData['password']),
            ]);

            return response()->json(['status' => 'success', 'message' => 'Register Successfully!']);
        }

        return response()->json(['status' => 'error', 'message' => 'Incorrect Code!']);
    }

    private function generateCode()
    {
        return substr(md5(uniqid(mt_rand(), true)), 0, 8);
    }

    private function sendEmail($validatedData, $code)
    {
        Mail::to(
            [
                'email' => $validatedData['email']
            ]
        )->send(new SendCodeRegister($code));
    }
}
