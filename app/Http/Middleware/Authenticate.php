<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Exceptions\HttpResponseException;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    // protected function redirectTo(Request $request): ?string
    // {
        
    // }

    protected function unauthenticated($request, array $guards)
    {
        throw new HttpResponseException(
            response()->json(['status' => 'error', 'message' => 'unathrnticated!!'], 400)
        );
    }
}
